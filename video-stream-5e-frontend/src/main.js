import { createApp } from 'vue'
import { createWebHistory, createRouter } from 'vue-router'
import App from './App.vue'
import VideoList from './VideoList.vue'
import WatchVideo from './WatchVideo.vue'

const routes = [
    { path: '/video-list', component: VideoList },
    { path: '/video/:id', component: WatchVideo },
]

const router = createRouter({
    // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
    history: createWebHistory(),
    routes, // short for `routes: routes`
    mode: 'history'
})
const app = createApp(App);


app.use(router)

app.mount('#app');
